import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Class3 {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String text;
        ArrayList<Integer> codesArray = new ArrayList<>();
        ArrayList<Integer> pricesArray = new ArrayList<>();
        String[] temp;
        int code;
        int codeIndex;

        do {
            System.out.println("enter a code and price of a product");

            text = reader.readLine();
            if (text.equals("stop"))
                break;
            temp = text.split(" ");
            codesArray.add(Integer.parseInt(temp[0]));
            pricesArray.add(Integer.parseInt(temp[1]));
        }
        while (!text.equals("stop"));

        System.out.println("enter a product code");
        code = Integer.parseInt(reader.readLine());
        if (!codesArray.contains(code))
            System.out.println("invalid code");
        else {
            codeIndex = codesArray.indexOf(code);
            System.out.println(pricesArray.get(codeIndex));
        }


    }
}

