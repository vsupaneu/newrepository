public class Class12 {
    public static void main(String[] args) {
        Test test1 = new Test();
        Test test2 = new Test();

        test1.addData("haha");
        test2.addData("hoho");
    }
}

class Test{

    public void addData(String info){
        info = this.getClass().toString() + " " + info;
        System.out.println(info);
    }


}
