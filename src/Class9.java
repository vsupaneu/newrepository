import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Class9 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(reader.readLine());

        long result = Factorial(a);
        System.out.println(result);

    }

    public static long Factorial(int number){
        long result = 1;
        for (int i = 1; i <= number; i++) {
            result *= i;
        }
        return result;
    }

}
