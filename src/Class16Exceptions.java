import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Class16Exceptions {

    private static String filePath = "C:\\test\\doc.txt";

    public static void main(String[] args){
        int a = 5, b = 0, c;
        String line;
        MyClass myClass1 = new MyClass("Gera");
        MyClass myClass2 = null;

        try{
            c = a * b;
        }
        finally {
            System.out.println("finally block");
        }

        //new comment

        try{
            System.out.println(myClass2.toString());
        }
        catch (NullPointerException e){
            System.out.println("NullPointerException");
        }

        System.out.println(myClass1.toString());

        try{
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            while ((line = br.readLine()) != null)
                System.out.println(line);
        }
        catch (IOException e){
            System.out.println("IO exception occured " + e.getMessage());
        }



    }
}

class MyClass{

    private String name;


    MyClass(String name){
        this.name = name;
    }

    MyClass(){}

    @Override
    public String toString(){
        return this.name;
    }
}