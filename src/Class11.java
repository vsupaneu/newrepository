import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class Class11 {
    public static void main(String[] args) throws IOException {

        int temp;
        Random random = new Random();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int number = Integer.parseInt(reader.readLine());

        for (int i = 0; i < number; i++) {
            temp = random.nextInt(10);
            if (temp == 5)
                System.out.println(temp);
        }

    }
}
