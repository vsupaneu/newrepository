import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Class6 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("заполните массив строк");

        String s = "";
        ArrayList<String> arrayList = new ArrayList<>();
        int counter = 0;

        while(!s.equals("stop")){
            s = reader.readLine();
            if (s.equals("stop"))
                break;
            arrayList.add(s);
        }

        for (String a : arrayList){
            arrayList.set(counter, a + " Hello");
            counter++;
        }

        for (String b : arrayList)
            System.out.println(b);


    }
}
